# ospfsh

Run ospfctl interactively in context of a given rdomain.
This crude scipt spares one from typing `ospfctl -s /var/run/ospfd.sock.<rdomain>` for every single command.

## Usage

```shell
ospfsh <rdomain>
```

Essentially the scripts appends all input to `ospfctl -s /var/run/ospfd.sock.<rdomain>`

Exit with ^D (EOF)
